// Intervals for fetching data and updating screen
// (in seconds)
var fetchInterval = 7 * 60; // 5 minutes
var updateInterval = 1; // 1 second

// Called onload
function main()
{
	// Initial fetch
	fetchDepartureData();

	// Setup update timers
	setInterval(fetchDepartureData, fetchInterval * 1000);
	setInterval(updateDepartureDisplay, updateInterval * 1000);
}
