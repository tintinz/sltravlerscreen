setInterval(function () {
  getTime();
}, 1000);

function getTime(){
  var today = new Date();
  // What happens next depends on whether you want UTC or locale time...
  // assuming locale time in this example...
  var sec = today.getSeconds();
  var min = today.getMinutes();
  var hour = today.getHours();
  if(sec<10){sec="0"+sec;}
  if(min<10){min="0"+min;}
  if(hour<10){hour="0"+hour;}
  document.getElementById("time").innerHTML =(hour+':'+min+':'+sec );
}
