var busData = [];
var trainData = [];

// Data structure holding info about a departure
function DepartureData(number, destination, time)
{
	this.number = number;
	this.destination = destination;
	this.time = Date.parse(time); // Time in milliseconds

	this.toTableString = function()
	{
		// Hide missed trains
		if (this.getRelativeTimeString() === "Passerad")
			return "";

		// Build table entry for departure
		var result = "";
		result =
			"<tr>" +
			"<td class = 'Numer-table'>" + this.number + "</td>" +
			"<td class = 'destination-table'>" + this.destination + "</td>" +
			"<td class = 'train-time'>" + this.getRelativeTimeString() + "</td>" +
			"</tr>";

		return result;
	};

	this.getRelativeTimeString = function()
	{
		var relativeTime = this.time - Date.now();
		var resultString = "";

		// From millisec to min
		relativeTime = Math.floor(relativeTime / 1000.0 / 60.0);
		if (relativeTime == 0)
		{
			resultString = "<1 min";
		}
		else if (relativeTime < 0)
		{
			resultString = "Passerad";
		}
		else
		{
			resultString = relativeTime.toString() + " min";
		}

		return resultString;
	}
}

// Parses JSON api-respose and updates data structures
function updateDepartureData(jsonData)
{
	console.log(jsonData);

	busData = parseDepatures(jsonData.ResponseData.Buses);
	trainData = parseDepatures(jsonData.ResponseData.Trains);

	updateDepartureDisplay();
}

// Parses departures from an specific json element (train/bus)
function parseDepatures(jsonElement)
{
	var result = [];

	for(var i=0; i<jsonElement.length; i++)
	{
		var number = jsonElement[i].LineNumber;
		var destination = jsonElement[i].Destination;
		var time = jsonElement[i].ExpectedDateTime;

		result.push(new DepartureData(number, destination, time));
	}

	return result;
}

// Updates the table displaying all departures
function updateDepartureDisplay()
{
	// Buses
	{
		var outString = "";
		outString += "<tr id='BUS'><th class = 'bus-title'>Buss</th><th class = 'bus-title-destination'>Mot</th><th class = 'bus-title-time'>Avgår</th></tr>"

		for(var i=0; i<busData.length; i++)
		{
			outString += busData[i].toTableString();
		}

		document.getElementById("bus").innerHTML = outString;
	}
	// Trains
	{
		var outString = "";
		outString += "<tr id='BUS'><th class = 'train-title'>Tåg</th><th class = 'train-title-destination'>Mot</th><th class = 'train-title-time'>Avgår</th></tr>"

		for(var i=0; i<trainData.length; i++)
		{
			outString += trainData[i].toTableString();
		}

		document.getElementById("train").innerHTML = outString;
	}
}
