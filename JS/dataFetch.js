function fetchDepartureData() {
	console.log("Fetching Departure Data...");

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		// Success
		if (this.readyState == 4 && this.status == 200) {
			console.log("Data Fetch Success");

			// Hook into departureTable.js
			updateDepartureData(JSON.parse(this.responseText));
		}
	};

	//xhttp.open("GET", "https://cors-anywhere.herokuapp.com/https://api.resrobot.se/v2/departureBoard?key=edc49b7b-3208-4a37-a80a-96a730202ebd&id=740000001&passlist=0&format=json", true);
	xhttp.open("GET", "https://cors-anywhere.herokuapp.com/http://api.sl.se/api2/realtimedeparturesV4.JSON?key=d39e05e76605466b875bb7cfdb9d6a86&siteid=9701&timewindow=120", true);
	//xhttp.open("GET", "realtimedeparturesV4.xml", true);
	//xhttp.open("GET", "https://api.resrobot.se/v2/departureBoard?key=854b2efd-cf6f-4c7a-8ff5-8c9696eb100e&id=")
	xhttp.send();
}
