<html>
<head>
  <link rel="stylesheet"href="https://fonts.googleapis.com/css?family=Open+Sans"/>

  <meta http-equiv="content-type" content="text/html" charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="CSS/top.css">
  <link rel="stylesheet" type="text/css" href="CSS/time.css">
  <link rel="stylesheet" type="text/css" href="CSS/adv.css">

  <script src="JS/dataFetch.js"></script>
  <script src="JS/getTime.js"></script>
  <script src="JS/departureTable.js"></script>
  <script src="JS/init.js"></script>
</head>

<body onload = "main()">
  <div class="top-bar">
    <div class="top-bar-text">

      <h1>SL - Kallhäll station</h1>

    </div>
    <div class="time" id="time"></div>
  </div>

  <div class="time-tables-icon">

    <div class="bus-icon">
      <img src="https://image.freepik.com/free-icon/front-of-bus_318-9306.jpg" alt="Mountain View" style="width:54px;height:58px;">
      <H1>Buss</H1>
    </div>

    <div class="train-icon">
      <img src="https://image.freepik.com/free-icon/train-front_318-61611.jpg" alt="Mountain View" style="width:54px;height:58px;">
      <H1>Tåg</H1>

    </div>


  </div>
  <div class="time-tables">
    <div class="buses">
      <table id="bus" class="bus"></table>
    </div>

    <div class="trains">
      <table id="train" class="train"></table>
    </div>
    </div>

    <p>
    <div class="Adv">

  <?php
    $arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);

      $url = 'https://www.ica.se/butiker/nara/jarfalla/ica-nara-kallhall-centrum-1158/butikserbjudanden/';

$content = file_get_contents($url, false, stream_context_create($arrContextOptions));
$first_step = explode('<section class="store-offers-carousel">' , $content );
$second_step = explode("</section>" , $first_step[1] );

echo $second_step[0];

      ?>
      <script type="text/javascript">
      	$(document).ready(function () {
      		$(".content").smoothDivScroll({
      			mousewheelScrolling: "allDirections",
      			manualContinuousScrolling: true,
      			autoScrollingMode: "onStart"
      		});
      	});
      </script>
    </div>
</p>

</body>
</html>
